/**
* Copyright: Copyright Auburn Sounds 2015-2017
* Copyright: Cut Through Recordings 2017
* License:   $(LINK2 http://www.boost.org/LICENSE_1_0.txt, Boost License 1.0)
* Authors:   Guillaume Piolat, Ethan Reker
*/
module main;

import std.math;
import std.algorithm;

import ddsp.osc;
import ddsp.util;
import ddsp.effect;
import ddsp.util.oversample;

import dplug.core,
       dplug.client;

import gui;

mixin(DLLEntryPoint!());

version(VST)
{
    import dplug.vst;
    mixin(VSTEntryPoint!ClipitClient);
}

version(AU)
{
    import dplug.au;
    mixin(AUEntryPoint!ClipitClient);
}

enum : int
{
    paramGain1,
    paramGain2,
    paramFreq1,
    paramFreq2,
    paramFreq3,
    paramNormal1,
    paramNormal2,
    paramNormal3,
    paramNormal4
}

final class ClipitClient : dplug.client.Client
{
public:
nothrow:
@nogc:

    this()
    {
        oversampler = calloc!(OverSampler!float).numChannels(2);
        distorter = calloc!Distorter.numChannels(2);
        oversampler[0].insertEffect(distorter[0]);
        oversampler[1].insertEffect(distorter[1]);

    }

    override PluginInfo buildPluginInfo()
    {
        static immutable PluginInfo pluginInfo = parsePluginInfo(import("plugin.json"));
        return pluginInfo;
    }

    override Parameter[] buildParameters()
    {
        auto params = makeVec!Parameter();
        params.pushBack( mallocNew!LinearFloatParameter(paramGain1, "Gain 1", "dB", -12.0f, 12.0f, 0.0f) );
        params.pushBack( mallocNew!LinearFloatParameter(paramGain2, "Gain 2", "dB", -12.0f, 12.0f, 0.0f) );
        params.pushBack( mallocNew!LinearFloatParameter(paramFreq1, "Freq 1", "Hz", 1.0f, 22500.0f, 1000.0f) );
        params.pushBack( mallocNew!LinearFloatParameter(paramFreq2, "Rate", "Hz", 0.02f, 5.0f, 0.18f) );
        params.pushBack( mallocNew!LinearFloatParameter(paramFreq3, "Offset", "Hz", 0.0f, 20.0f, 0.0f) );
        params.pushBack( mallocNew!LinearFloatParameter(paramNormal1, "Depth", "", 0.0f, 1.0f, 0.0f) );
        params.pushBack( mallocNew!LinearFloatParameter(paramNormal2, "Mix", "", 0.0f, 1.0f, 0.0f) );
        params.pushBack( mallocNew!LinearFloatParameter(paramNormal3, "Feedback", "", 0.0f, 1.0f, 0.0f) );
        params.pushBack( mallocNew!LinearFloatParameter(paramNormal4, "Normal 4", "", 0.0f, 1.0f, 0.0f) );
        return params.releaseData();
    }

    override LegalIO[] buildLegalIO()
    {
        auto io = makeVec!LegalIO();
        io.pushBack(LegalIO(1, 1));
        io.pushBack(LegalIO(1, 2));
        io.pushBack(LegalIO(2, 1));
        io.pushBack(LegalIO(2, 2));
        return io.releaseData();
    }

    override int maxFramesInProcess() const
    {
        return 512;
    }

    override void reset(double sampleRate, int maxFrames, int numInputs, int numOutputs) nothrow @nogc
    {
        foreach(chan; 0..2)
        {
            oversampler[chan].setSampleFactor(2);
            oversampler[chan].setSampleRate(sampleRate);
        }
        assert(maxFrames <= 512);
    }

    override void processAudio(const(float*)[] inputs, float*[]outputs, int frames,
                               TimeInfo info) nothrow @nogc
    {
        assert(frames <= 512);

        int numInputs = cast(int)inputs.length;
        int numOutputs = cast(int)outputs.length;

        int minChan = numInputs > numOutputs ? numOutputs : numInputs;

        for (int chan = 0; chan < minChan; ++chan)
        {
            for (int f = 0; f < frames; ++f)
            {
                float inputSample = inputs[chan][f];
                outputs[chan][f] = oversampler[chan].getNextSample(inputSample);
            }
        }

        for (int chan = minChan; chan < numOutputs; ++chan)
            outputs[chan][0..frames] = 0;
    }

    override IGraphics createGraphics()
    {
        return mallocNew!ClipitGUI(this);
    }

private:
    OverSampler!float[] oversampler;
    Distorter[] distorter;
}

class Distorter : AudioEffect
{
    private import std.math;
    override float getNextSample(const float input) { return sin(input);}
    override void reset() {}
}